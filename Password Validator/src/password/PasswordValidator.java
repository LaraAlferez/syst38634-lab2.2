/*
 *@author Lara Alferez, 991540084  
 * This class validates passwords and it will be developed using TDD
 */

package password;

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;
	
	public static boolean isValidLength( String password ) {
		return  password.length() >= MIN_LENGTH;
	}
	
	public static boolean hasReguiredDigits( String password ) {
		int digits = 2;
		return  digits <= MIN_NUM_DIGITS ;
	}
		
}
