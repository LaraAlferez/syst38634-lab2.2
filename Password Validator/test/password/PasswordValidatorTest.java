/*
 *@author Lara Alferez, 991540084  
 * This class validates testing passwords methods and it will be developed using TDD
 */
package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(""));
	}

	@Test
	public void testIsValidLengthBoundaryIn() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(""));
	}

	@Test
	public void testIsValidLengthBoundaryOut() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("123456789"));
	}

	@Test
	public void testHasRequiredDigitsRegular() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12abcdef"));
	}
	
	@Test
	public void testHasRequiredDigitsException() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("123bcdef"));
	}
	
	@Test
	public void testHasRequiredDigitsBoundaryIn() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12abcdef"));
	}
	
	@Test
	public void testHasRequiredDigitsBoundaryOut() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12345678"));
	}

}
